INPUT_FILENAME = 'source.json'
TAGS_MAP = {
    'title': 'h1',
    'body': 'p'
}
BASE_LIST_TAG = 'ul'
LIST_ELEMENT_TAG = 'li'
