import settings
from html_element import HTMLElement


def build_structure_elements(data_sctructure):
    """
    recursively iterate through elements structure, build HTMLElement instances
    """
    if isinstance(data_sctructure, list):
        # main list element (ul, by default)
        base_element = HTMLElement(tag=settings.BASE_LIST_TAG)
        for list_item in data_sctructure:
            # list element (li, by default)
            list_element = HTMLElement(tag=settings.LIST_ELEMENT_TAG)
            # build and append subelements to list element
            for subelement in build_structure_elements(list_item):
                list_element.add_child(subelement)
            base_element.add_child(list_element)
        yield base_element
    elif isinstance(data_sctructure, dict):
        for tag, content in data_sctructure.items():
            # case tag not specified (task #1)
            if tag in settings.TAGS_MAP:
                tag = settings.TAGS_MAP[tag]
            if isinstance(content, list):
                element = HTMLElement(tag=tag)
                for subelement in build_structure_elements(content):
                    element.add_child(subelement)
            else:
                element = HTMLElement(tag=tag, content=content)
            yield element
