import os
import sys
import json
import settings
from utils import build_structure_elements

file_content = None
output = ''

# check file exists
if not os.path.isfile(settings.INPUT_FILENAME):
    sys.exit('ERROR: File %s not found' % settings.INPUT_FILENAME)

# read file content
with open(settings.INPUT_FILENAME, 'r') as input_file:
    try:
        file_content = json.load(input_file)
    except json.JSONDecodeError:
        sys.exit('ERROR: Wrong file content format')

# get elements output
for element in build_structure_elements(file_content):
    output += element.render()

print(output)
