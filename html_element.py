import re
import xml.etree.ElementTree as ET


class HTMLElement:
    def __init__(self, tag, content=None):
        self.tag = tag
        self.content = content
        self.children = []

    def add_child(self, child):
        if type(self) == type(child):
            self.children.append(child)

    def _build_et_element(self):
        """
        create xml.etree.ElementTree.Element instances for object and it's
        children
        """
        tag, attributes = self._build_tag_attributes()
        xml_element = ET.Element(tag, attributes)
        xml_element.text = self.content
        if self.children:
            for child in self.children:
                xml_element.append(child._build_et_element())
        return xml_element

    def _build_tag_attributes(self):
        """
        parse id and class attributes from tag name, clean tag name
        """
        classes = []
        id = None
        attributes = {}
        tag = self.tag

        class_names_regexp = re.compile(r'(\.[a-zA-Z0-9\-_]+)')
        id_regexp = re.compile(r'#([a-zA-Z0-9\-_]+)')

        for class_match in class_names_regexp.finditer(tag):
            class_name = class_match.group(1)
            tag = tag.replace(class_name, '')
            classes.append(class_name.lstrip('.'))

        id_match = id_regexp.search(tag)
        if id_match:
            id = id_match.group(1)
            tag = tag.replace(id, '').replace('#', '')

        if classes:
            attributes['class'] = ' '.join(classes)
        if id:
            attributes['id'] = id

        return (tag, attributes)

    def render(self):
        """
        return string interpretation of element
        """
        xml_element = self._build_et_element()
        return ET.tostring(xml_element, encoding='unicode')
