import os
import json
import unittest
from abc import ABC
from utils import build_structure_elements

class AbstarctOutputTest(ABC):
    base_input_folfer = 'test_fixtures/input/'
    base_output_folfer = 'test_fixtures/output/'

    def test_output_equal(self):
        input_path = os.path.join(self.base_input_folfer, self.input_filename)
        with open(input_path, 'r') as input_file:
            input = json.load(input_file)

        output_path = os.path.join(self.base_output_folfer, self.output_filename)
        with open(output_path, 'r') as output_file:
            output = output_file.read()

        project_results = ''
        for element in build_structure_elements(input):
            project_results += element.render()
        self.assertEqual(project_results.strip(), output.strip())


class TestTask1Output(AbstarctOutputTest, unittest.TestCase):
    input_filename = 'task1.json'
    output_filename = 'task1.html'


class TestTask2Output(AbstarctOutputTest, unittest.TestCase):
    input_filename = 'task2.json'
    output_filename = 'task2.html'


class TestTask3Output(AbstarctOutputTest, unittest.TestCase):
    input_filename = 'task3.json'
    output_filename = 'task3.html'


class TestTask4Output(AbstarctOutputTest, unittest.TestCase):
    input_filename = 'task4.json'
    output_filename = 'task4.html'


class TestTask4v2Output(AbstarctOutputTest, unittest.TestCase):
    input_filename = 'task4v2.json'
    output_filename = 'task4v2.html'
